Rails.application.routes.draw do
  resources :foods
  get 'static_pages/about'
  get 'static_pages/User'
  get 'static_pages/Proposal'
  root 'static_pages#about'
end
