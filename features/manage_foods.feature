Feature: Manage foods
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new food
    Given I am on the new food page
    When I fill in "Food" with "food 1"
    And I fill in "Detail" with "detail 1"
    And I fill in "Calorlies" with "calorlies 1"
    And I fill in "Fat" with "fat 1"
    And I fill in "Cholesterol" with "cholesterol 1"
    And I fill in "Sodium" with "sodium 1"
    And I fill in "Carbohyfrate" with "carbohyfrate 1"
    And I fill in "Protein" with "protein 1"
    And I fill in "Vitamin" with "vitamin 1"
    And I fill in "Bitamin" with "bitamin 1"
    And I fill in "Calcium" with "Calcium 1"
    And I fill in "Iron" with "iron 1"
    And I press "Create"
    Then I should see "food 1"
    And I should see "detail 1"
    And I should see "calorlies 1"
    And I should see "fat 1"
    And I should see "cholesterol 1"
    And I should see "sodium 1"
    And I should see "carbohyfrate 1"
    And I should see "protein 1"
    And I should see "vitamin 1"
    And I should see "bitamin 1"
    And I should see "Calcium 1"
    And I should see "iron 1"

  Scenario: Delete food
    Given the following foods:
      |food|detail|calorlies|fat|cholesterol|sodium|carbohyfrate|protein|vitamin|bitamin|Calcium|iron|
      |food 1|detail 1|calorlies 1|fat 1|cholesterol 1|sodium 1|carbohyfrate 1|protein 1|vitamin 1|bitamin 1|Calcium 1|iron 1|
      |food 2|detail 2|calorlies 2|fat 2|cholesterol 2|sodium 2|carbohyfrate 2|protein 2|vitamin 2|bitamin 2|Calcium 2|iron 2|
      |food 3|detail 3|calorlies 3|fat 3|cholesterol 3|sodium 3|carbohyfrate 3|protein 3|vitamin 3|bitamin 3|Calcium 3|iron 3|
      |food 4|detail 4|calorlies 4|fat 4|cholesterol 4|sodium 4|carbohyfrate 4|protein 4|vitamin 4|bitamin 4|Calcium 4|iron 4|
    When I delete the 3rd food
    Then I should see the following foods:
      |Food|Detail|Calorlies|Fat|Cholesterol|Sodium|Carbohyfrate|Protein|Vitamin|Bitamin|Calcium|Iron|
      |food 1|detail 1|calorlies 1|fat 1|cholesterol 1|sodium 1|carbohyfrate 1|protein 1|vitamin 1|bitamin 1|Calcium 1|iron 1|
      |food 2|detail 2|calorlies 2|fat 2|cholesterol 2|sodium 2|carbohyfrate 2|protein 2|vitamin 2|bitamin 2|Calcium 2|iron 2|
      |food 4|detail 4|calorlies 4|fat 4|cholesterol 4|sodium 4|carbohyfrate 4|protein 4|vitamin 4|bitamin 4|Calcium 4|iron 4|
