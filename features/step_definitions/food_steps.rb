Given /^the following foods:$/ do |foods|
  Food.create!(foods.hashes)
end

When /^I delete the (\d+)(?:st|nd|rd|th) food$/ do |pos|
  visit foods_path
  within("table tr:nth-child(#{pos.to_i+1})") do
    click_link "Destroy"
  end
end

Then /^I should see the following foods:$/ do |expected_foods_table|
  rows=find("table").all('tr')
  table=row.map{|r|r.all('th,td').map{|c|c.text_strip}}
  expected_foods_table.diff!(table)
end
