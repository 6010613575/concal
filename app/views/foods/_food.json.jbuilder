json.extract! food, :id, :food, :detail, :calories, :fat, :cholesterol, :sodium, :carbohydrate, :protein, :vitamin, :bitamin, :Calcium, :iron, :created_at, :updated_at
json.url food_url(food, format: :json)
