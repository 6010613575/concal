# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_03_194101) do

  create_table "foods", force: :cascade do |t|
    t.string "food"
    t.text "detail"
    t.decimal "calories", precision: 8, scale: 2
    t.decimal "fat", precision: 8, scale: 2
    t.decimal "cholesterol", precision: 8, scale: 2
    t.decimal "sodium", precision: 8, scale: 2
    t.decimal "carbohydrate", precision: 8, scale: 2
    t.decimal "protein", precision: 8, scale: 2
    t.decimal "vitamin", precision: 8, scale: 2
    t.decimal "bitamin", precision: 8, scale: 2
    t.decimal "Calcium", precision: 8, scale: 2
    t.decimal "iron", precision: 8, scale: 2
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
