class CreateFoods < ActiveRecord::Migration[6.0]
  def change
    create_table :foods do |t|
      t.string :food
      t.text :detail
      t.decimal :calories, precision: 8, scale: 2
      t.decimal :fat, precision: 8, scale: 2
      t.decimal :cholesterol, precision: 8, scale: 2
      t.decimal :sodium, precision: 8, scale: 2
      t.decimal :carbohydrate, precision: 8, scale: 2
      t.decimal :protein, precision: 8, scale: 2
      t.decimal :vitamin, precision: 8, scale: 2
      t.decimal :bitamin, precision: 8, scale: 2
      t.decimal :Calcium, precision: 8, scale: 2
      t.decimal :iron, precision: 8, scale: 2

      t.timestamps
    end
  end
end
