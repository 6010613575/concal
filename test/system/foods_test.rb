require "application_system_test_case"

class FoodsTest < ApplicationSystemTestCase
  setup do
    @food = foods(:one)
  end

  test "visiting the index" do
    visit foods_url
    assert_selector "h1", text: "Foods"
  end

  test "creating a Food" do
    visit foods_url
    click_on "New Food"

    fill_in "Calcium", with: @food.Calcium
    fill_in "Bitamin", with: @food.bitamin
    fill_in "Calories", with: @food.calories
    fill_in "Carbohydrate", with: @food.carbohydrate
    fill_in "Cholesterol", with: @food.cholesterol
    fill_in "Detail", with: @food.detail
    fill_in "Fat", with: @food.fat
    fill_in "Food", with: @food.food
    fill_in "Iron", with: @food.iron
    fill_in "Protein", with: @food.protein
    fill_in "Sodium", with: @food.sodium
    fill_in "Vitamin", with: @food.vitamin
    click_on "Create Food"

    assert_text "Food was successfully created"
    click_on "Back"
  end

  test "updating a Food" do
    visit foods_url
    click_on "Edit", match: :first

    fill_in "Calcium", with: @food.Calcium
    fill_in "Bitamin", with: @food.bitamin
    fill_in "Calories", with: @food.calories
    fill_in "Carbohydrate", with: @food.carbohydrate
    fill_in "Cholesterol", with: @food.cholesterol
    fill_in "Detail", with: @food.detail
    fill_in "Fat", with: @food.fat
    fill_in "Food", with: @food.food
    fill_in "Iron", with: @food.iron
    fill_in "Protein", with: @food.protein
    fill_in "Sodium", with: @food.sodium
    fill_in "Vitamin", with: @food.vitamin
    click_on "Update Food"

    assert_text "Food was successfully updated"
    click_on "Back"
  end

  test "destroying a Food" do
    visit foods_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Food was successfully destroyed"
  end
end
